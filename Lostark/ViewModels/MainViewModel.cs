﻿using Microsoft.Win32;
using Lostark.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Lostark.View;
using Lostark.Models;
using GalaSoft.MvvmLight.Messaging;
using System.Threading;
using System.Timers;
using System.Globalization;

namespace Lostark.viewmodel
{
    public class MainViewModel : INotifyPropertyChanged
    {
        public MainViewModel()
        {
            AddCharacterCmd = new RelayCommand(AddCharacterCmdExe, CanAddCharacterCmdExe);
            EditCharacterCmd = new RelayCommand(EditCharacterCmdExe, CanEditCharacterCmdExe);
            SaveCharacterCmd = new RelayCommand(SaveCharacterCmdExe, CanSaveCharacterCmdExe);
            DeleteCharacterCmd = new RelayCommand(DeleteCharacterCmdExe, CanDeleteCharacterCmdExe);
            ShowCharacterWorkCmd = new RelayCommand(ShowCharacterWorkCmdExe, CanShowCharacterWorkCmdExe);

            DailyWorkCmd = new RelayCommand(DailyWorkCmdExe, CanDailyWorkCmdExe);
            ChangeChecked_DailyWorkCmd = new RelayCommand(ChangeChecked_DailyWorkCmdExe, CanChangeChecked_DailyWorkCmdExe);
            WeeklyWorkCmd = new RelayCommand(WeeklyWorkCmdExe, CanWeeklyWorkCmdExe);
            ChangeChecked_WeeklyWorkCmd = new RelayCommand(ChangeChecked_WeeklyWorkCmdExe, CanChangeChecked_WeeklyWorkCmdExe);

            Delete_WeeklyWorkCmd = new RelayCommand(Delete_WeeklyWorkCmdExe, CanDelete_WeeklyWorkCmdExe);
            Delete_DailyWorkCmd = new RelayCommand(Delete_DailyWorkCmdExe, CanDelete_DailyWorkCmdExe);

            AddDailyWorkCmd = new RelayCommand(AddDailyWorkCmdExe, CanAddDailyWorkCmdExe);
            AddWeeklyWorkCmd = new RelayCommand(AddWeeklyWorkCmdExe, CanAddWeeklyWorkCmdExe);

            ChangeChecked_IslandMindCmd = new RelayCommand(ChangeChecked_IslandMindCmdExe, CanChangeChecked_IslandMindCmdExe);

            RefreshWeeklyWorkCmd = new RelayCommand(RefreshWeeklyWorkCmdExe, CanRefreshWeeklyWorkCmdExe);
            RefreshDailyWorkCmd = new RelayCommand(RefreshDailyWorkCmdExe, CanRefreshDailyWorkCmdExe);

            GameDataManager.Instance.Initialize();
            UserDataManager.Instance.Initialize();

            UpdateCharacterInfo();
            UpdateIslandMind();

            //CharacterInfos = new ObservableCollection<CharacterInfo>();
            //dtCharacter_info = DatabaseManager.Instance.GetCharacterInfo();
            //dtCharacter_info2 = DatabaseManager.Instance.GetCharacterInfo2();
            //DatabaseManager.Instance.Initialize();

            ClearWorkDoneTimer = new System.Timers.Timer(60000);
            ClearWorkDoneTimer.Elapsed += ClearWorkDoneTimerEvent;
            ClearWorkDoneTimer.Enabled = true;

        }

        public ObservableCollection<UserCharacterStructor> User_Characters { get; set; }
        public UserCharacterStructor Selected_Character { get; set; }
        public void UpdateCharacterInfo()
        {
            User_Characters = new ObservableCollection<UserCharacterStructor>(UserDataManager.Instance.GetUserCharacter());
            UpdateWorkList();
            RaisePropertyChanged("User_Characters");
        }
        #region ----------------------------------- ClearWorkDoneTimer  --------------------------------------
        System.Timers.Timer ClearWorkDoneTimer;
        private void ClearWorkDoneTimerEvent(Object source, ElapsedEventArgs e)
        {
            if (UserDataManager.Instance.GetUserCharacter() == null) return;
            TimeSpan timeDiff = DateTime.Now - AppConfigManager.Instance.config.DailyWorkLastClearTime;
            //TimeSpan timeDiff = DateTime.Now  - (new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day - 1, DateTime.Now.Hour, 0, 0));

            if (timeDiff.Days >= 2)
            {
                AppConfigManager.Instance.config.DailyWorkLastClearTime = DateTime.Now;
                AppConfigManager.Instance.config.IsDailyWorkClear = true;
                foreach (var c in UserDataManager.Instance.GetUserCharacter())
                {
                    c.ClearDailyWorkDone();
                }
                UpdateWorkList();
                UserDataManager.Instance.SaveUserCharacter();
                UpdateCharacterInfo();
                AppConfigManager.Instance.SaveConfig();
            }
            else
            {
                int hour = AppConfigManager.Instance.config.DailyWorkLastClearTime.Hour;
                int week = (int)AppConfigManager.Instance.config.DailyWorkLastClearTime.DayOfWeek;
                int min = AppConfigManager.Instance.config.DailyWorkLastClearTime.Minute;

                for (int i = 0; i < (int)timeDiff.TotalMinutes; i++)
                {
                    min++;
                    if (min >= 60)
                    {
                        min = 0;
                        hour++;
                        if (hour >= 24)
                        {
                            hour = 0; week++;
                            if (week >= 7) week = 0;
                        }
                    }
                    //if (hour == 15 && min > 0)
                    if (hour == 6 && min == 0)
                    {
                        AppConfigManager.Instance.config.DailyWorkLastClearTime = DateTime.Now;
                        foreach (var c in UserDataManager.Instance.GetUserCharacter())
                        {
                            c.ClearDailyWorkDone();
                        }
                        UpdateWorkList();
                        UserDataManager.Instance.SaveUserCharacter();
                        UpdateCharacterInfo();
                        AppConfigManager.Instance.SaveConfig();
                        break;
                    }
                }
            }

            TimeSpan timeDiff2 = DateTime.Now - AppConfigManager.Instance.config.WeeklyWorkLastClearTime;
            if (timeDiff2.Days >= 8)
            {
                AppConfigManager.Instance.config.WeeklyWorkLastClearTime = DateTime.Now;
                foreach (var c in UserDataManager.Instance.GetUserCharacter())
                {
                    c.ClearWeeklyWorkDone();
                }
                UpdateWorkList();
                UserDataManager.Instance.SaveUserCharacter();
                UpdateCharacterInfo();
                AppConfigManager.Instance.SaveConfig();
            }
            else// if(timeDiff2.Days > 0)
            {
                int hour = AppConfigManager.Instance.config.WeeklyWorkLastClearTime.Hour;
                int week = (int)AppConfigManager.Instance.config.WeeklyWorkLastClearTime.DayOfWeek;
                int min = AppConfigManager.Instance.config.WeeklyWorkLastClearTime.Minute;

                for (int i = 0; i < (int)timeDiff2.TotalMinutes; i++)
                {
                    min++;
                    if (min >= 60)
                    {
                        min = 0;
                        hour++;
                        if (hour >= 24)
                        {
                            hour = 0; week++;
                            if (week >= 7) week = 0;
                        }
                    }
                    if ((week == 3) && (hour == 6))
                    {
                        AppConfigManager.Instance.config.WeeklyWorkLastClearTime = DateTime.Now;
                        foreach (var c in UserDataManager.Instance.GetUserCharacter())
                        {
                            c.ClearWeeklyWorkDone();
                        }
                        UpdateWorkList();
                        UserDataManager.Instance.SaveUserCharacter();
                        UpdateCharacterInfo();
                        AppConfigManager.Instance.SaveConfig();
                        break;
                    }

                }
            }

        }
        #endregion
        #region ----------------------------------- island mind --------------------------------------
        public ObservableCollection<s_IslandMind> Island_Mind { get; set; }

        public void UpdateIslandMind()
        {
            try
            {
                Island_Mind = new ObservableCollection<s_IslandMind>(GameDataManager.Instance.island_mind_info);
            }
            catch
            {
            
            }

            
        }
        public RelayCommand ChangeChecked_IslandMindCmd { get; set; }
        void ChangeChecked_IslandMindCmdExe(object param)
        {

        }
        bool CanChangeChecked_IslandMindCmdExe(object param)
        {
            return true;
        }
        #endregion
        #region --------------------------------- Daily work ------------------------------------------------

        public RelayCommand Delete_DailyWorkCmd { get; set; }
        void Delete_DailyWorkCmdExe(object param)
        {
            if (Selected_Daily_Work == null)
            {
                MessageBox.Show("삭제할 항목을 선택해주세요");
                return;
            }
            var result = MessageBox.Show("해당 항목 삭제하시겠습니까?", "삭제", MessageBoxButton.YesNo);

            if (result == MessageBoxResult.Yes)
            {
                Selected_Character_Work.DeleteDailyWorks(Selected_Daily_Work);
                UpdateWorkList();
                UserDataManager.Instance.SaveUserCharacter();
            }
        }
        bool CanDelete_DailyWorkCmdExe(object param)
        {
            return true;
        }
        public Daily_Work Selected_Daily_Work { get; set; }
        public RelayCommand DailyWorkCmd { get; set; }
        public ObservableCollection<Daily_Work> Show_Daily_Work { get; set; }

        void DailyWorkCmdExe(object param)
        {
            if (Selected_Daily_Work == null) return;
            if (Selected_Character_Work == null) return;

            if (Selected_Daily_Work.item == "에포나")
            {
                EditEponaWindow _wEditEponaWindow = new EditEponaWindow();
                Messenger.Default.Send<s_epona_quest_work>(Selected_Character_Work.works.epona_quest[Selected_Daily_Work.index]);

                if (_wEditEponaWindow.ShowDialog() == false)
                {
                    UpdateWorkList();
                    UserDataManager.Instance.SaveUserCharacter();
                    UpdateCharacterInfo();
                }
            }
            else if (Selected_Daily_Work.item == "가디언토벌")
            {
                Message_Gadian_raid msg = new Message_Gadian_raid(Selected_Character_Work, Selected_Daily_Work);
                GadianRaidWindow w = new GadianRaidWindow();
                Messenger.Default.Send<Message_Gadian_raid>(msg);
                if (w.ShowDialog() == false)
                {

                }
            }
            else if (Selected_Daily_Work.item == "카오스던전")
            {
                Message_Chaose_Dungeon msg = new Message_Chaose_Dungeon()
                { 
                    select_character = Selected_Character_Work,
                    select_work = Selected_Daily_Work
                };

                ChaosDungeonWindow w = new ChaosDungeonWindow();
                Messenger.Default.Send<Message_Chaose_Dungeon>(msg);
                if (w.ShowDialog() == false)
                {

                }
            }
        }
        bool CanDailyWorkCmdExe(object param)
        {
            return true;
        }

        public RelayCommand ChangeChecked_DailyWorkCmd { get; set; }
        void ChangeChecked_DailyWorkCmdExe(object param)
        {
            Selected_Character_Work.SetDailyWorks(Selected_Daily_Work);
            UserDataManager.Instance.SaveUserCharacter();
            UpdateCharacterInfo();
        }
        bool CanChangeChecked_DailyWorkCmdExe(object param)
        {
            return true;
        }
        #endregion
        #region --------------------------------- Weekly work ------------------------------------------------
        public RelayCommand Delete_WeeklyWorkCmd { get; set; }
        void Delete_WeeklyWorkCmdExe(object param)
        {
            if (Selected_Weekly_Work == null)
            {
                MessageBox.Show("삭제할 항목을 선택해주세요");
                return;
            }
            var result = MessageBox.Show("해당 항목 삭제하시겠습니까?", "삭제", MessageBoxButton.YesNo);

            if (result == MessageBoxResult.Yes)
            {
                Selected_Character_Work.DeleteWeeklyWorks(Selected_Weekly_Work);
                UpdateWorkList();
                UserDataManager.Instance.SaveUserCharacter();
            }
        }
        bool CanDelete_WeeklyWorkCmdExe(object param)
        {
            return true;
        }

        public Weekly_Work Selected_Weekly_Work { get; set; }
        public RelayCommand WeeklyWorkCmd { get; set; }
        public ObservableCollection<Weekly_Work> Show_Weekly_Work { get; set; }

        void WeeklyWorkCmdExe(object param)
        {
            if (Selected_Weekly_Work == null) return;
            if (Selected_Character_Work == null) return;

            if (Selected_Weekly_Work.item == "어비스레이드")
            {
                Message_Abyss_raid msg = new Message_Abyss_raid(Selected_Character_Work, Selected_Weekly_Work);
                AbyssRaidWindow w = new AbyssRaidWindow();
                Messenger.Default.Send<Message_Abyss_raid>(msg);
                if (w.ShowDialog() == false)
                {

                }
            }
            else if (Selected_Weekly_Work.item == "어비스던전")
            {
                Message_Abyss_Dungeon msg = new Message_Abyss_Dungeon()
                {
                    select_character = Selected_Character_Work,
                    select_work = Selected_Weekly_Work
                };

                AbyssDungeonWindow w = new AbyssDungeonWindow();
                Messenger.Default.Send<Message_Abyss_Dungeon>(msg);
                if (w.ShowDialog() == false)
                {

                }
            }
        }
        bool CanWeeklyWorkCmdExe(object param)
        {
            return true;
        }

        public RelayCommand ChangeChecked_WeeklyWorkCmd { get; set; }
        void ChangeChecked_WeeklyWorkCmdExe(object param)
        {
            Selected_Character_Work.SetWeeklyWorks(Selected_Weekly_Work);
            UserDataManager.Instance.SaveUserCharacter();
            UpdateCharacterInfo();
        }
        bool CanChangeChecked_WeeklyWorkCmdExe(object param)
        {
            return true;
        }
        #endregion
        #region --------------------------------- show character work ------------------------------------------------
        public UserCharacterStructor Selected_Character_Work { get; set; }
        
        public RelayCommand ShowCharacterWorkCmd { get; set; }
        
        public void UpdateWorkList()
        {
            if (Selected_Character_Work == null) return;
            //Selected_Character_Work.Check_Weekly_Works();
            //Selected_Character_Work.Check_Daily_Works();
            Show_Weekly_Work = new ObservableCollection<Weekly_Work>(Selected_Character_Work.GetWeeklyWorks());
            Show_Daily_Work = new ObservableCollection<Daily_Work>(Selected_Character_Work.GetDailyWorks());
            RaisePropertyChanged("Show_Weekly_Work");
            RaisePropertyChanged("Show_Daily_Work");
        }

        void ShowCharacterWorkCmdExe(object param)
        {
            UpdateWorkList();
            ClearWorkDoneTimerEvent(null, null);
        }
        bool CanShowCharacterWorkCmdExe(object param)
        {
            return true;
        }

        #endregion
        #region --------------------------------- Work add commands ------------------------------------------------
        public RelayCommand AddWeeklyWorkCmd { get; set; }

        void AddWeeklyWorkCmdExe(object param)
        {
            AddWorksWindow w = new AddWorksWindow();
            Message_AddWorks message_AddWorks = new Message_AddWorks()
            {
                select_character = Selected_Character_Work,
                work_type = "weekly"

            };

            Messenger.Default.Send<Message_AddWorks>(message_AddWorks);

            if (w.ShowDialog() == false)
            {
                UserDataManager.Instance.SaveUserCharacter();
                UpdateCharacterInfo();
            }

        }
        bool CanAddWeeklyWorkCmdExe(object param)
        {
            return true;
        }
        public RelayCommand AddDailyWorkCmd { get; set; }

        void AddDailyWorkCmdExe(object param)
        {
            AddWorksWindow w = new AddWorksWindow();
            Message_AddWorks message_AddWorks = new Message_AddWorks()
            {
                select_character = Selected_Character_Work,
                work_type = "daily"
            };

            Messenger.Default.Send<Message_AddWorks>(message_AddWorks);

            if (w.ShowDialog() == false)
            {
                UserDataManager.Instance.SaveUserCharacter();
                UpdateCharacterInfo();
            }

        }
        bool CanAddDailyWorkCmdExe(object param)
        {
            return true;
        }
        #endregion
        #region --------------------------------- Work refresh commands ------------------------------------------------
        public RelayCommand RefreshWeeklyWorkCmd { get; set; }

        void RefreshWeeklyWorkCmdExe(object param)
        {
            if (Selected_Character_Work == null) return;

            UserDataManager.Instance.Initialize_work_abyss_raid(Selected_Character_Work);
            UserDataManager.Instance.Initialize_work_abyss_dungeon(Selected_Character_Work);
            UserDataManager.Instance.Initialize_work_challenge_abyss_dungeon(Selected_Character_Work);
            UserDataManager.Instance.Initialize_work_challenge_gadian_raid(Selected_Character_Work);
            UserDataManager.Instance.Initialize_work_guild_raid(Selected_Character_Work);

            UserDataManager.Instance.SaveUserCharacter();
            UpdateCharacterInfo();
        }
        bool CanRefreshWeeklyWorkCmdExe(object param)
        {
            return true;
        }
        public RelayCommand RefreshDailyWorkCmd { get; set; }

        void RefreshDailyWorkCmdExe(object param)
        {
            if (Selected_Character_Work == null) return;

            UserDataManager.Instance.Initialize_work_chaos_dungeon(Selected_Character_Work);
            UserDataManager.Instance.Initialize_work_gadian_raid(Selected_Character_Work);
            UserDataManager.Instance.Initialize_work_epona_quest(Selected_Character_Work);

            UserDataManager.Instance.SaveUserCharacter();
            UpdateCharacterInfo();
        }
        bool CanRefreshDailyWorkCmdExe(object param)
        {
            return true;
        }
        #endregion
        #region --------------------------------- CharacterEdit commands ------------------------------------------------
        public RelayCommand EditCharacterCmd { get; set; }

        void EditCharacterCmdExe(object param)
        {
            CharacterEditWindow CharacterEditWindow = new CharacterEditWindow();
            Messenger.Default.Send<UserCharacterStructor>(Selected_Character);

            if (CharacterEditWindow.ShowDialog() == false)
            {
                UserDataManager.Instance.SaveUserCharacter();
                UpdateCharacterInfo();
            }

        }
        bool CanEditCharacterCmdExe(object param)
        {
            return true;
        }
        #endregion
        #region CharacterAdd
        public RelayCommand AddCharacterCmd { get; set; }
        
        void AddCharacterCmdExe(object param)
        {
            CharacterEditWindow CharacterEditWindow = new CharacterEditWindow();
            if (CharacterEditWindow.ShowDialog() == false)
            {
                UserDataManager.Instance.SaveUserCharacter();
                UpdateCharacterInfo();
            }
            
        }
        bool CanAddCharacterCmdExe(object param)
        {
            return true;
        }


        public RelayCommand DeleteCharacterCmd { get; set; }
        void DeleteCharacterCmdExe(object param)
        {
            if (Selected_Character == null)
            {
                MessageBox.Show("삭제할 캐릭터를 선택해주세요");
            }
            else
            {
                var result = MessageBox.Show("해당 캐릭터를 삭제하시겠습니까?", "삭제", MessageBoxButton.YesNo);

                if (result == MessageBoxResult.Yes)
                {
                    UserDataManager.Instance.Delete_Chracter(Selected_Character);
                    UserDataManager.Instance.SaveUserCharacter();
                    UpdateCharacterInfo();
                }
                else
                {

                }
                

            }

            
        }
        bool CanDeleteCharacterCmdExe(object param)
        {
            return true;
        }

        #endregion
        #region Character delete
        public RelayCommand SaveCharacterCmd { get; set; }
        void SaveCharacterCmdExe(object param)
        {
            //DatabaseManager.Instance.UpdateCharacterInfo(dtCharacter_info);
        }
        bool CanSaveCharacterCmdExe(object param)
        {
            return true;
        }
        #endregion
        #region command
        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(string propertyName)
        {
            // take a copy to prevent thread issues
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion
    }


    public class Diff_Offense_Power
    {
        public string item { get; set; }
        public double offense_power_0 { get; set; }
        public double offense_power_20 { get; set; }
        public double offense_power_35 { get; set; }
        public double offense_power_50 { get; set; }

        public Diff_Offense_Power()
        {

        }
    }


}
