﻿using GalaSoft.MvvmLight.Messaging;
using Lostark.Command;
using Lostark.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lostark.viewmodel
{
    public class EditEponaViewModel : INotifyPropertyChanged
    {
        public EditEponaViewModel()
        {
            SaveInfoCmd = new RelayCommand(SaveInfoCmdExe, CanSaveInfoCmdExe);

            Messenger.Default.Register<s_epona_quest_work>
            (
                    this,
                    (action) => ReceiveMessage(action)
            );
        }

        public s_epona_quest_work Received_Action { get; set; }
        public bool Is_Ignored { get; set; }
        public string Name { get; set; }
        public string Note { get; set; }

        private void ReceiveMessage(s_epona_quest_work action)
        {
            if (action == null) return;
            
            Received_Action = action;

            Is_Ignored = action.is_ignore;
            Name = action.info.quest_name;
            Note = action.info.note;

            RaisePropertyChanged("Is_Ignored");
            RaisePropertyChanged("Name");
            RaisePropertyChanged("Note");
        }

        public RelayCommand SaveInfoCmd { get; set; }
        void SaveInfoCmdExe(object param)
        {
            if (Received_Action == null) return;

            Received_Action.is_ignore = Is_Ignored;
            Received_Action.info.quest_name = Name;
            Received_Action.info.note = Note;

        }

        bool CanSaveInfoCmdExe(object param)
        {
            return true;
        }


        #region command
        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(string propertyName)
        {
            // take a copy to prevent thread issues
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion

    }
}
