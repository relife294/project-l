﻿using GalaSoft.MvvmLight.Messaging;
using Lostark.Command;
using Lostark.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Lostark.viewmodel
{
    public class Message_Abyss_Dungeon
    {
        public UserCharacterStructor select_character;
        public Weekly_Work select_work;

        public Message_Abyss_Dungeon()
        {
        }
    }

    public class AbyssDungeonViewModel : INotifyPropertyChanged
    {
        public AbyssDungeonViewModel()
        {
            SaveInfoCmd = new RelayCommand(SaveInfoCmdExe, CanSaveInfoCmdExe);
            Diff_offens_power = new ObservableCollection<Diff_Offense_Power>();
            Messenger.Default.Register<Message_Abyss_Dungeon>
            (
                    this,
                    (action) => ReceiveMessage(action)
            );
        }
        public Message_Abyss_Dungeon Received_Message { get; set; }
        public string Note { get; set; }
        public string Reward { get; set; }
        public string Name { get; set; }
        public int Tier { get; set; }
        public bool Is_Ignored { get; set; }
        public double Raid_Level { get; set; }
        public double Character_Offense_Power { get; set; }
        public string Difficulty { get; set;  }
        public ObservableCollection<Diff_Offense_Power> Diff_offens_power { get; set; }
        private void ReceiveMessage(Message_Abyss_Dungeon action)
        {
            if (action == null) return;
            if (action.select_character == null) return;
            if (action.select_work == null) return;

            Received_Message = action;
            Is_Ignored = action.select_work.is_ignored;
            foreach (var raid in GameDataManager.Instance.abyss_dungeon_info)
            {
                if ((action.select_work.name == raid.name) && (action.select_work.name2 == raid.difficulty))
                {
                    Name = raid.name;
                    Tier = raid.tier;
                    Raid_Level = raid.level_min;
                    Difficulty = raid.difficulty;

                    Diff_Offense_Power raid_op = new Diff_Offense_Power
                    {
                        item = "레이드",
                        offense_power_0 = raid.offense_power_0,
                        offense_power_20 = raid.offense_power_20,
                        offense_power_35 = raid.offense_power_35,
                        offense_power_50 = raid.offense_power_50,
                    };

                    Character_Offense_Power = action.select_character.offense_power;
                    Diff_Offense_Power character_op = new Diff_Offense_Power
                    {
                        item = "공격력미달치",
                        offense_power_0 = raid.offense_power_0 > 0 ? Character_Offense_Power - raid.offense_power_0 : 0,
                        offense_power_20 = raid.offense_power_20 > 0 ? Character_Offense_Power - raid.offense_power_20 : 0,
                        offense_power_35 = raid.offense_power_35 > 0 ? Character_Offense_Power - raid.offense_power_35 : 0,
                        offense_power_50 = raid.offense_power_50 > 0 ? Character_Offense_Power - raid.offense_power_50 : 0,
                    };
                    Diff_offens_power.Add(raid_op);
                    Diff_offens_power.Add(character_op);

                    Note = raid.GetNote();
                    Reward = raid.GetReward();
                    
                    break;
                }
            }

            RaisePropertyChanged("Name");
            RaisePropertyChanged("Tier");
            RaisePropertyChanged("Raid_Level");
            RaisePropertyChanged("Character_Offense_Power");
            RaisePropertyChanged("Diff_offens_power");
            RaisePropertyChanged("Is_Ignored");
            RaisePropertyChanged("Note");
            RaisePropertyChanged("Reward");
            RaisePropertyChanged("Difficulty");
        }

        public RelayCommand SaveInfoCmd { get; set; }
        void SaveInfoCmdExe(object param)
        {
            if (Received_Message == null) return;
            if (Received_Message.select_character == null) return;
            if (Received_Message.select_work == null) return;

            Received_Message.select_work.is_ignored = Is_Ignored;
            foreach (var raid in GameDataManager.Instance.abyss_dungeon_info)
            {
                if((Received_Message.select_work.name == raid.name) && (Received_Message.select_work.name2 == raid.difficulty))
                {
                    raid.SetNote(Note);
                    raid.SetReward(Reward);

                    Received_Message.select_character.SetWeeklyWorks(Received_Message.select_work);
                    UserDataManager.Instance.SaveUserCharacter();
                    break;
                }
            }

        }
        bool CanSaveInfoCmdExe(object param)
        {
            return true;
        }
        #region command
        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(string propertyName)
        {
            // take a copy to prevent thread issues
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion
    }


}
