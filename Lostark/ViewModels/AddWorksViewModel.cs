﻿using GalaSoft.MvvmLight.Messaging;
using Lostark.Command;
using Lostark.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace Lostark.viewmodel
{
    public class Message_AddWorks
    {
        public UserCharacterStructor select_character;
        public string work_type;
        public Weekly_Work weekly_work;
        public Daily_Work daily_work;

        public Message_AddWorks()
        {
        }

    }
    public class AddWorksViewModel : INotifyPropertyChanged
    {
        string[] Item_DailyWork = { "가디언토벌", "에포나", "카오스던전"};
        string[] Item_WeeklyWork = { "어비스던전", "어비스레이드", "도전 가디언토벌", "도전 어비스던전", "길드토벌"};

        public AddWorksViewModel()
        {

            SaveInfoCmd = new RelayCommand(SaveInfoCmdExe, CanSaveInfoCmdExe);

            WorkType2 = new ObservableCollection<string>();
            WorkType3 = new ObservableCollection<string>();

            Messenger.Default.Register<Message_AddWorks>
            (
                    this,
                    (action) => ReceiveMessage(action)
            );
        }
        public Message_AddWorks Received_Message { get; set; }
        public ObservableCollection<string> WorkType { get; set; }
        public ObservableCollection<string> WorkType2 { get; set; }
        public ObservableCollection<string> WorkType3 { get; set; }

        string _selected_WorkType;
        public string Selected_WorkType 
        {
            get
            {
                return _selected_WorkType;
            }
            set
            {
                if (value != _selected_WorkType)
                {
                    _selected_WorkType = value;
                    UpdateWorkType2(value);
                    RaisePropertyChanged("Selected_WorkType");
                }
            }
        }
        string _selected_WorkType2;
        public string Selected_WorkType2
        {
            get
            {
                return _selected_WorkType2;
            }
            set
            {
                if (value != _selected_WorkType2)
                {
                    _selected_WorkType2 = value;
                    UpdateWorkType3(Selected_WorkType, value);
                    RaisePropertyChanged("Selected_WorkType2");
                }
            }
        }
        public string Selected_WorkType3 { get; set; }
        private void ReceiveMessage(Message_AddWorks action)
        {
            if (action == null) return;
            if (action.select_character == null) return;
            if (action.work_type == "") return;

            Received_Message = action;

            if (action.work_type == "weekly")
            {
                WorkType = new ObservableCollection<string>(Item_WeeklyWork);
            }
            else if (action.work_type == "daily")
            {
                WorkType = new ObservableCollection<string>(Item_DailyWork);
            }

            RaisePropertyChanged("WorkType");
        }

        public void UpdateWorkType2(string item)
        {
            WorkType2.Clear();
            switch (item)
            {
                case "에포나":
                    {

                    }
                    break;
                case "가디언토벌":
                    {
                        for (int i = 0; i < GameDataManager.Instance.gadian_raid_info.Count; i++)
                        {
                            if (WorkType2.Contains(GameDataManager.Instance.gadian_raid_info[i].name)) continue;
                            WorkType2.Add(GameDataManager.Instance.gadian_raid_info[i].name);
                        }
                    }
                    break;
                case "카오스던전":
                    {
                        for (int i = 0; i < GameDataManager.Instance.chaos_dungeon_info.Count; i++)
                        {
                            if (WorkType2.Contains(GameDataManager.Instance.chaos_dungeon_info[i].name)) continue;
                            WorkType2.Add(GameDataManager.Instance.chaos_dungeon_info[i].name);
                        }
                    }
                    break;
                case "어비스레이드":
                    {
                        for (int i = 0; i < GameDataManager.Instance.abyss_raid_info.Count; i++)
                        {
                            if (WorkType2.Contains(GameDataManager.Instance.abyss_raid_info[i].name)) continue;
                            WorkType2.Add(GameDataManager.Instance.abyss_raid_info[i].name);
                        }
                    }
                    break;
                case "어비스던전":
                    {
                        for (int i = 0; i < GameDataManager.Instance.abyss_dungeon_info.Count; i++)
                        {
                            if (WorkType2.Contains(GameDataManager.Instance.abyss_dungeon_info[i].name)) continue;
                            WorkType2.Add(GameDataManager.Instance.abyss_dungeon_info[i].name);
                        }
                    }
                    break;
                case "도전 가디언토벌":
                    {
                        for (int i = 0; i < GameDataManager.Instance.gadian_raid_info.Count; i++)
                        {
                            if (WorkType2.Contains(GameDataManager.Instance.gadian_raid_info[i].name)) continue;
                            WorkType2.Add(GameDataManager.Instance.gadian_raid_info[i].name);
                        }
                    }
                    break;
                case "도전 어비스던전":
                    {
                        for (int i = 0; i < GameDataManager.Instance.abyss_dungeon_info.Count; i++)
                        {
                            if (WorkType2.Contains(GameDataManager.Instance.abyss_dungeon_info[i].name)) continue;
                            WorkType2.Add(GameDataManager.Instance.abyss_dungeon_info[i].name);
                        }
                    }
                    break;
                case "길드토벌":
                    {

                    }
                    break;
            }
            RaisePropertyChanged("WorkType2");
        }
        public void UpdateWorkType3(string item1, string item2)
        {
            WorkType3.Clear();
            switch (item1)
            {
                case "에포나":
                    {

                    }
                    break;
                case "가디언토벌":
                    {

                    }
                    break;
                case "카오스던전":
                    {
                        for (int i = 0; i < GameDataManager.Instance.chaos_dungeon_info.Count; i++)
                        {
                            if (GameDataManager.Instance.chaos_dungeon_info[i].name == item2)
                            {
                                WorkType3.Add(GameDataManager.Instance.chaos_dungeon_info[i].difficulty);
                            }
                        }
                    }
                    break;
                case "어비스레이드":
                    {
                        for (int i = 0; i < GameDataManager.Instance.abyss_raid_info.Count; i++)
                        {
                            if (GameDataManager.Instance.abyss_raid_info[i].name == item2)
                            {
                                WorkType3.Add(GameDataManager.Instance.abyss_raid_info[i].phase);
                            }
                        }
                    }
                    break;
                case "어비스던전":
                    {
                        for (int i = 0; i < GameDataManager.Instance.abyss_dungeon_info.Count; i++)
                        {
                            if (GameDataManager.Instance.abyss_dungeon_info[i].name == item2)
                            {
                                WorkType3.Add(GameDataManager.Instance.abyss_dungeon_info[i].difficulty);
                            }
                        }
                    }
                    break;
                case "도전 가디언토벌":
                    {
                    }
                    break;
                case "도전 어비스던전":
                    {
                    }
                    break;
                case "길드토벌":
                    {

                    }
                    break;
            }
            RaisePropertyChanged("WorkType3");
        }
        public RelayCommand SaveInfoCmd { get; set; }
        void SaveInfoCmdExe(object param)
        {
            if (Received_Message == null) return;
            if (Received_Message.select_character == null) return;
            if (Selected_WorkType == null) return;

            switch (Selected_WorkType)
            {
                case "에포나":
                    {
                        Daily_Work work = new Daily_Work()
                        {
                            item = Selected_WorkType,
                            name = "",
                        };
                        Received_Message.select_character.AddDailyWorks(work);
                    }
                    break;
                case "가디언토벌":
                    {
                        if (Selected_WorkType2 == null) return;
                        Daily_Work work = new Daily_Work()
                        {
                            item = Selected_WorkType,
                            name = Selected_WorkType2,
                        };
                        Received_Message.select_character.AddDailyWorks(work);
                    }
                    break;
                case "카오스던전":
                    {
                        if (Selected_WorkType2 == null) return;
                        if (Selected_WorkType3 == null) return;

                        Daily_Work work = new Daily_Work()
                        {
                            item = Selected_WorkType,
                            name = Selected_WorkType2,
                            name2 = Selected_WorkType3,
                        };
                        Received_Message.select_character.AddDailyWorks(work);
                    }
                    break;
                case "어비스레이드":
                    {
                        if (Selected_WorkType2 == null) return;
                        if (Selected_WorkType3 == null) return;

                        Weekly_Work work = new Weekly_Work()
                        {
                            item = Selected_WorkType,
                            name = Selected_WorkType2,
                            name2 = Selected_WorkType3,
                        };
                        Received_Message.select_character.AddWeeklyWorks(work);
                    }
                    break;
                case "어비스던전":
                    {
                        if (Selected_WorkType2 == null) return;
                        if (Selected_WorkType3 == null) return;

                        Weekly_Work work = new Weekly_Work()
                        {
                            item = Selected_WorkType,
                            name = Selected_WorkType2,
                            name2 = Selected_WorkType3,
                        };
                        Received_Message.select_character.AddWeeklyWorks(work);
                    }
                    break;
                case "도전 가디언토벌":
                    {
                    }
                    break;
                case "도전 어비스던전":
                    {
                    }
                    break;
                case "길드토벌":
                    {

                    }
                    break;
            }
        }
        bool CanSaveInfoCmdExe(object param)
        {
            return true;
        }
        #region command
        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(string propertyName)
        {
            // take a copy to prevent thread issues
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion
    }


}
