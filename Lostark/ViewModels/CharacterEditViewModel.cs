﻿using Microsoft.Win32;
using Lostark.Command;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Data;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using Lostark.View;
using Lostark.Models;
using GalaSoft.MvvmLight.Messaging;

namespace Lostark.viewmodel
{
    public class CharacterEditViewModel : INotifyPropertyChanged
    {
        
        public CharacterEditViewModel()
        {
            EditCharacterCmd = new RelayCommand(EditCharacterCmdExe, CanEditCharacterCmdExe);
            JobMember = new ObservableCollection<s_job_info>(GameDataManager.Instance.job_info);
            TierMember = new ObservableCollection<int>(new List<int>() { 1, 2, 3 });


            Messenger.Default.Register<UserCharacterStructor>
            (
                 this,
                 (action) => ReceiveMessage(action)
            );

        }
        bool isEditCharacterWindow = false;
        public UserCharacterStructor Receive_Selected_Character { get; set; }

        public int Selected_TierMember { get; set; }
        public ObservableCollection<int> TierMember { get; set; }

        #region Add

        public ObservableCollection<s_job_info> JobMember { get; set; }
        public s_job_info Selected_JobMember { get; set; }

        public string Character_Name { get; set; }
        public double Character_ItemLevel { get; set; }
        public double Character_offense_power { get; set; }

        private void ReceiveMessage(UserCharacterStructor action)
        {
            if (action == null) return;
            isEditCharacterWindow = true;
            Receive_Selected_Character = action;
            Selected_JobMember = JobMember.Where(x => x.name.Contains(action.job.name)).FirstOrDefault();
            Character_Name = action.name; 
            Character_ItemLevel = action.item_level;
            Character_offense_power = action.offense_power;
            Selected_TierMember = TierMember.Where(x => x == action.tier).FirstOrDefault();
            RaisePropertyChanged("Character_Name");
            RaisePropertyChanged("Character_ItemLevel");
            RaisePropertyChanged("Character_offense_power");
            RaisePropertyChanged("Selected_JobMember");
            RaisePropertyChanged("Selected_TierMember");
        }

        public RelayCommand EditCharacterCmd { get; set; }
        void EditCharacterCmdExe(object param)
        {
            if (isEditCharacterWindow == false)
            {
                UserDataManager.Instance.Add_Character(Character_Name, Selected_JobMember, Convert.ToDouble(Character_ItemLevel), Character_offense_power, Selected_TierMember);
            }
            else
            {
                UserDataManager.Instance.Change_Character(Receive_Selected_Character, Character_Name, Selected_JobMember, Convert.ToDouble(Character_ItemLevel), Character_offense_power, Selected_TierMember);
            }
            
        }
        bool CanEditCharacterCmdExe(object param)
        {
            return true;
        }

        #endregion
        #region command
        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(string propertyName)
        {
            // take a copy to prevent thread issues
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
        #endregion
    }



}
