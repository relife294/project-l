﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Lostark.Models
{
    public class AppConfig
    {
        public DateTime DailyWorkLastClearTime;
        public DateTime WeeklyWorkLastClearTime;

        public bool IsWeeklyWorkClear;
        public bool IsDailyWorkClear;
    }

    public class AppConfigManager
    {
        private static readonly Lazy<AppConfigManager> instance = new Lazy<AppConfigManager>(() => new AppConfigManager());
        //public static 의 객체반환 함수
        public static AppConfigManager Instance { get { return instance.Value; } }

        private AppConfigManager()
        {
            config = new AppConfig();
            LoadConfig();
        }
        const string app_config_path = "game_info/app_config";

        public AppConfig config;

        public void SaveConfig()
        {
            using (FileStream outFile = File.Create(app_config_path))
            {
                XmlSerializer formatter = new XmlSerializer(config.GetType());
                formatter.Serialize(outFile, config);
            }

        }
        public void LoadConfig()
        {
            try
            {
                XmlSerializer formatter = new XmlSerializer(config.GetType());
                using (FileStream aFile = new FileStream(app_config_path, FileMode.Open))
                {
                    byte[] buffer = new byte[aFile.Length];
                    aFile.Read(buffer, 0, (int)aFile.Length);
                    MemoryStream stream = new MemoryStream(buffer);
                    config = (AppConfig)formatter.Deserialize(stream);
                }
            }
            catch
            {

            }
        }


    }
}
