﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lostark.Models
{


    public class s_job_info
    {
        public int index { get; set; }
        public string name { get; set; }

        public double hp_ratio { get; set; }
        public double defence_ratio { get; set; }

        public s_job_info (int index, string name, double hp_ratio, double defence_ratio)
        {
            this.index = index;
            this.name = name;
            this.hp_ratio = hp_ratio;
            this.defence_ratio = defence_ratio;
        }
        public s_job_info(s_job_info s)
        {
            index = s.index;
            name = s.name;
            hp_ratio = s.hp_ratio;
            defence_ratio = s.defence_ratio;
        }

        public s_job_info()
        {

        }
    }
    public class s_epona_quest_info
    {
        public int index;

        public string quest_name;

        public string note;

        public s_epona_quest_info()
        {
        }
    }


    public class s_abyss_dungeon_info
    {
        public int index { get; set; }
        public string name { get; set; }
        public string difficulty { get; set; }
        public int tier { get; set; }

        public double level_min { get; set; }
        public double level_max { get; set; }

        public double offense_power_0 { get; set; }
        public double offense_power_20 { get; set; }
        public double offense_power_35 { get; set; }
        public double offense_power_50 { get; set; }

        private string reward { get; set; }
        private string note { get; set; }


        public void SetReward(string reward)
        {
            try
            {
                this.reward = reward;
                using (var writer = new StreamWriter($"game_info/Reward/{name}_{difficulty}_Reward.txt", false))
                {
                    writer.Write(reward);
                }
            }
            catch
            {

            }

        }
        public string GetReward()
        {
            try
            {
                using (var reader = new StreamReader($"game_info/Reward/{name}_{difficulty}_Reward.txt"))
                {
                    reward = reader.ReadToEnd();
                }
            }
            catch
            {
            }

            return reward;
        }
        public void SetNote(string note)
        {
            try
            {
                this.note = note;
                using (var writer = new StreamWriter($"game_info/Note/{name}_{difficulty}_Note.txt", false))
                {
                    writer.Write(note);
                }
            }
            catch
            {

            }
        }
        public string GetNote()
        {
            try
            {
                using (var reader = new StreamReader($"game_info/Note/{name}_{difficulty}_Note.txt"))
                {
                    note = reader.ReadToEnd();
                }
            }
            catch
            {

            }
            return this.note;
        }
        public s_abyss_dungeon_info()
        {
        }

    }


    public class s_abyss_raid_info
    {
        public int index { get; set; }
        public string name { get; set; }
        public string phase { get; set; }
        public int tier { get; set; }

        public double level_min { get; set; }
        public double level_max { get; set; }

        public double offense_power_0 { get; set; }
        public double offense_power_20 { get; set; }
        public double offense_power_35 { get; set; }
        public double offense_power_50 { get; set; }

        private string reward { get; set; }
        private string note { get; set; }

        public s_abyss_raid_info()
        {
        }

        public void SetReward(string reward)
        {
            try
            {
                this.reward = reward;
                using (var writer = new StreamWriter($"game_info/Reward/{name}_{phase}_Reward.txt", false))
                {
                    writer.Write(reward);
                }
            }
            catch
            {

            }

        }
        public string GetReward()
        {
            try
            {
                using (var reader = new StreamReader($"game_info/Reward/{name}_{phase}_Reward.txt"))
                {
                    reward = reader.ReadToEnd();
                }
            }
            catch
            {
            }

            return reward;
        }
        public void SetNote(string note)
        {
            try
            {
                this.note = note;
                using (var writer = new StreamWriter($"game_info/Note/{name}_{phase}_Note.txt", false))
                {
                    writer.Write(note);
                }
            }
            catch
            {

            }
        }
        public string GetNote()
        {
            try
            {
                using (var reader = new StreamReader($"game_info/Note/{name}_{phase}_Note.txt"))
                {
                    note = reader.ReadToEnd();
                }
            }
            catch
            {

            }
            return this.note;
        }

    }

    public class s_chaos_dungeon_info
    {
        public int index { get; set; }
        public string name { get; set; }
        public string difficulty { get; set; }
        public int tier { get; set; }

        public double level_min { get; set; }
        public double level_max { get; set; }

        public double offense_power_0 { get; set; }
        public double offense_power_20 { get; set; }
        public double offense_power_35 { get; set; }
        public double offense_power_50 { get; set; }

        private string reward { get; set; }
        private string note { get; set; }

        public s_chaos_dungeon_info()
        {
        }

        public void SetReward(string reward)
        {
            try
            {
                this.reward = reward;
                using (var writer = new StreamWriter($"game_info/Reward/{name}_{difficulty}_Reward.txt", false))
                {
                    writer.Write(reward);
                }
            }
            catch
            {

            }

        }
        public string GetReward()
        {
            try
            {
                using (var reader = new StreamReader($"game_info/Reward/{name}_{difficulty}_Reward.txt"))
                {
                    reward = reader.ReadToEnd();
                }
            }
            catch
            {
            }

            return reward;
        }
        public void SetNote(string note)
        {
            try
            {
                this.note = note;
                using (var writer = new StreamWriter($"game_info/Note/{name}_{difficulty}_Note.txt", false))
                {
                    writer.Write(note);
                }
            }
            catch
            {

            }
        }
        public string GetNote()
        {
            try
            {
                using (var reader = new StreamReader($"game_info/Note/{name}_{difficulty}_Note.txt"))
                {
                    note = reader.ReadToEnd();
                }
            }
            catch
            {

            }
            return this.note;
        }

    }



    public class s_gadian_raid_info
    {
        public int index { get; set; }
        public string name { get; set; }
        public int tier { get; set; }

        public double level_min { get; set; }
        public double level_max { get; set; }

        public double offense_power_0 { get; set; }
        public double offense_power_20 { get; set; }
        public double offense_power_35 { get; set; }
        public double offense_power_50 { get; set; }

        private string reward { get; set; }
        private string note { get; set; }

        public s_gadian_raid_info()
        {
        }

        public void SetReward(string reward)
        {
            try
            {
                this.reward = reward;
                using (var writer = new StreamWriter($"game_info/Reward/{name}_Reward.txt", false))
                {
                    writer.Write(reward);
                }
            }
            catch
            {

            }

        }
        public string GetReward()
        {
            try
            {
                using (var reader = new StreamReader($"game_info/Reward/{name}_Reward.txt"))
                {
                    reward = reader.ReadToEnd();
                }
            }
            catch
            {
            }

            return reward;
        }
        public void SetNote(string note)
        {
            try
            {
                this.note = note;
                using (var writer = new StreamWriter($"game_info/Note/{name}_Note.txt", false))
                {
                    writer.Write(note);
                }
            }
            catch
            {

            }
        }
        public string GetNote()
        {
            try
            {
                using (var reader = new StreamReader($"game_info/Note/{name}_Note.txt"))
                {
                    note = reader.ReadToEnd();
                }
            }
            catch
            {

            }
            return this.note;
        }
    }



    public class s_guild_raid_info
    {
        public int index { get; set; }
        public string name { get; set; }

        public s_guild_raid_info()
        {
        }
    }
    public class s_challenge_gadian_raid_info
    {
        public int index { get; set; }
        public string name { get; set; }

        public s_challenge_gadian_raid_info()
        {
        }
    }
    public class s_challenge_abyss_dungeon_info
    {
        public int index { get; set; }
        public string name { get; set; }

        public s_challenge_abyss_dungeon_info()
        {
        }

    }
}
