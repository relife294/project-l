﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Lostark.Models
{
    public class UserDataManager
    {
        private static readonly Lazy<UserDataManager> instance = new Lazy<UserDataManager>(() => new UserDataManager());
        //public static 의 객체반환 함수
        public static UserDataManager Instance { get { return instance.Value; } }

        private UserDataManager()
        {
        }

        const string user_config_path = "game_info/user/user_character";
        List<UserCharacterStructor> user_character = new List<UserCharacterStructor>();

        public void Initialize()
        {
            LoadUserCharacter();
            //Initialize_works();
            Refresh_work_challenge_gadian_raid();
            Refresh_work_challenge_abyss_dungeon();
        }
        public void SaveUserCharacter()
        {
            using (FileStream outFile = File.Create(user_config_path))
            {
                XmlSerializer formatter = new XmlSerializer(user_character.GetType());
                formatter.Serialize(outFile, user_character);
            }
        }
        public void LoadUserCharacter()
        {
            try
            {
                XmlSerializer formatter = new XmlSerializer(user_character.GetType());
                using (FileStream aFile = new FileStream(user_config_path, FileMode.Open))
                {
                    byte[] buffer = new byte[aFile.Length];
                    aFile.Read(buffer, 0, (int)aFile.Length);
                    MemoryStream stream = new MemoryStream(buffer);
                    user_character = (List<UserCharacterStructor>)formatter.Deserialize(stream);
                }
            }
            catch
            {

            }
        }

        public void Add_Character(string name, s_job_info job, double item_level, double offense_power,int tier)
        {
            user_character.Add(new UserCharacterStructor(name, new s_job_info(job), item_level, offense_power, tier));
            Initialize_character_works(user_character.Last());
        }
        public void Change_Character(UserCharacterStructor uc, string name, s_job_info job, double item_level, double offense_power, int tier)
        {
            //UserCharacterStructor character = user_character.Where(x => x.name.Contains(name)).FirstOrDefault();
            uc.name = name;
            uc.job = new s_job_info(job);
            uc.item_level = item_level;
            uc.offense_power = offense_power;
            uc.tier = tier;
            //UserDataManager.Instance.Initialize_character_works(uc);

            //user_character.Add(new UserCharacterStructor(name, new s_job_info(job), item_level, offense_power));
        }
        public void Delete_Chracter(UserCharacterStructor s)
        {
            user_character.Remove(s);
        }
        public List<UserCharacterStructor> GetUserCharacter()
        {
            return user_character;
        }
        public void Refresh_work_challenge_gadian_raid()
        {
            foreach (var character in user_character)
            {
                for (int i = 0; i < GameDataManager.Instance.challenge_gadian_raid_info.Count; i++)
                {
                    if (character.works.challenge_gadian_raid[i] == null) continue;
                    character.works.challenge_gadian_raid[i].info = GameDataManager.Instance.challenge_gadian_raid_info[i];
                }
            }
        }
        public void Refresh_work_challenge_abyss_dungeon()
        {
            foreach(var character in user_character)
            {
                for (int i = 0; i < GameDataManager.Instance.challenge_abyss_dungeon_info.Count; i++)
                {
                    if (character.works.challenge_adyss_dungeon[i] == null) continue;
                    character.works.challenge_adyss_dungeon[i].info = GameDataManager.Instance.challenge_abyss_dungeon_info[i];
                }
            }
        }
        public void Initialize_work_challenge_gadian_raid(UserCharacterStructor character)
        {
            character.works.challenge_gadian_raid.Clear();
            for (int i = 0; i < GameDataManager.Instance.challenge_gadian_raid_info.Count; i++)
            {
                character.works.challenge_gadian_raid.Add(new s_challenge_gadian_raid_work()
                {
                    info = GameDataManager.Instance.challenge_gadian_raid_info[i],
                });
            }
        }
        public void Initialize_work_challenge_abyss_dungeon(UserCharacterStructor character)
        {
            character.works.challenge_adyss_dungeon.Clear();
            for (int i = 0; i < GameDataManager.Instance.challenge_abyss_dungeon_info.Count; i++)
            {
                character.works.challenge_adyss_dungeon.Add(new s_challenge_abyss_dungeon_work()
                {
                    info = GameDataManager.Instance.challenge_abyss_dungeon_info[i],
                });

            }
        }
        public void Initialize_work_guild_raid(UserCharacterStructor character)
        {
            character.works.guild_raid.Clear();
            for (int i = 0; i < GameDataManager.Instance.guild_raid_info.Count; i++)
            {
                character.works.guild_raid.Add(new s_guild_raid_work()
                {
                    info = GameDataManager.Instance.guild_raid_info[i],
                });
            }
        }

        #region-------------------------------- 아이템 레벨에 따른 어비스 레이드 리스트 업 --------------------------------
        // 어비스 레이드는 최종 1페/2페... 등등 만
        public void Initialize_work_abyss_raid(UserCharacterStructor character)
        {
            character.works.abyss_raid.Clear();
            for (int i = 0; i < GameDataManager.Instance.abyss_raid_info.Count; i++)
            {
                double item_level = character.item_level;
                double offense_power = character.offense_power;

                double item_level_min = GameDataManager.Instance.abyss_raid_info[i].level_min;
                double item_level_max = GameDataManager.Instance.abyss_raid_info[i].level_max;
                double offense_power_0 = GameDataManager.Instance.abyss_raid_info[i].offense_power_0;
                double offense_power_20 = GameDataManager.Instance.abyss_raid_info[i].offense_power_20;
                double offense_power_35 = GameDataManager.Instance.abyss_raid_info[i].offense_power_35;
                double offense_power_50 = GameDataManager.Instance.abyss_raid_info[i].offense_power_50;

                if (((item_level >= item_level_min) && (item_level < item_level_max)) || ((item_level >= item_level_min) && (item_level_max == 0)))
                {
                    s_abyss_raid_work work = new s_abyss_raid_work();

                    if ((offense_power >= offense_power_50) && (offense_power_50 != 0))         work.offense_power = "공증 50%";
                    else if ((offense_power >= offense_power_35) && (offense_power_35 != 0))    work.offense_power = "공증 35%";
                    else if ((offense_power >= offense_power_20) && (offense_power_20 != 0))    work.offense_power = "공증 20%";
                    else if ((offense_power >= offense_power_0) && (offense_power_0 != 0))     work.offense_power = "공증 0%";
                    else if ((offense_power_0 == 0) && (offense_power_20 == 0) && (offense_power_35 == 0) && (offense_power_50 == 0)) work.offense_power = "공컷 정보 없음";
                    else                                            work.offense_power = "공컷 미달";

                    work.info = GameDataManager.Instance.abyss_raid_info[i];

                    character.works.abyss_raid.Add(work);
                }
            }
        }
        public void Add_work_abyss_raid(UserCharacterStructor character, s_abyss_raid_info info)
        {
            for (int i = 0; i < GameDataManager.Instance.abyss_raid_info.Count; i++)
            {
                if ((GameDataManager.Instance.abyss_raid_info[i].name != info.name) ||
                    (GameDataManager.Instance.abyss_raid_info[i].phase != info.phase))
                {
                    continue;
                }

                double item_level = character.item_level;
                double offense_power = character.offense_power;

                double item_level_min = GameDataManager.Instance.abyss_raid_info[i].level_min;
                double item_level_max = GameDataManager.Instance.abyss_raid_info[i].level_max;
                double offense_power_0 = GameDataManager.Instance.abyss_raid_info[i].offense_power_0;
                double offense_power_20 = GameDataManager.Instance.abyss_raid_info[i].offense_power_20;
                double offense_power_35 = GameDataManager.Instance.abyss_raid_info[i].offense_power_35;
                double offense_power_50 = GameDataManager.Instance.abyss_raid_info[i].offense_power_50;

                //if (((item_level >= item_level_min) && (item_level < item_level_max)) || ((item_level >= item_level_min) && (item_level_max == 0)))
                {
                    s_abyss_raid_work work = new s_abyss_raid_work();

                    if ((offense_power >= offense_power_50) && (offense_power_50 != 0)) work.offense_power = "공증 50%";
                    else if ((offense_power >= offense_power_35) && (offense_power_35 != 0)) work.offense_power = "공증 35%";
                    else if ((offense_power >= offense_power_20) && (offense_power_20 != 0)) work.offense_power = "공증 20%";
                    else if ((offense_power >= offense_power_0) && (offense_power_0 != 0)) work.offense_power = "공증 0%";
                    else if ((offense_power_0 == 0) && (offense_power_20 == 0) && (offense_power_35 == 0) && (offense_power_50 == 0)) work.offense_power = "공컷 정보 없음";
                    else work.offense_power = "공컷 미달";

                    work.info = GameDataManager.Instance.abyss_raid_info[i];

                    character.works.abyss_raid.Add(work);
                }
            }
        }
        #endregion
        #region-------------------------------- 아이템 레벨에 따른 어비스 던전 리스트 업 --------------------------------
        public void Initialize_work_abyss_dungeon(UserCharacterStructor character)
        {
            character.works.abyss_dungeon.Clear();
            for (int i = 0; i < GameDataManager.Instance.abyss_dungeon_info.Count; i++)
            {
                double item_level = character.item_level;
                double offense_power = character.offense_power;

                double item_level_min = GameDataManager.Instance.abyss_dungeon_info[i].level_min;
                double item_level_max = GameDataManager.Instance.abyss_dungeon_info[i].level_max;
                double offense_power_0 = GameDataManager.Instance.abyss_dungeon_info[i].offense_power_0;
                double offense_power_20 = GameDataManager.Instance.abyss_dungeon_info[i].offense_power_20;
                double offense_power_35 = GameDataManager.Instance.abyss_dungeon_info[i].offense_power_35;
                double offense_power_50 = GameDataManager.Instance.abyss_dungeon_info[i].offense_power_50;

                if (((item_level >= item_level_min) && (item_level < item_level_max)) || ((item_level >= item_level_min) && (item_level_max == 0)))
                {
                    s_abyss_dungeon_work work = new s_abyss_dungeon_work();

                    if ((offense_power >= offense_power_50) && (offense_power_50 != 0)) work.offense_power = "공증 50%";
                    else if ((offense_power >= offense_power_35) && (offense_power_35 != 0)) work.offense_power = "공증 35%";
                    else if ((offense_power >= offense_power_20) && (offense_power_20 != 0)) work.offense_power = "공증 20%";
                    else if ((offense_power >= offense_power_0) && (offense_power_0 != 0)) work.offense_power = "공증 0%";
                    else if ((offense_power_0 == 0) && (offense_power_20 == 0) && (offense_power_35 == 0) && (offense_power_50 == 0)) work.offense_power = "공컷 정보 없음";
                    else work.offense_power = "공컷 미달";

                    work.info = GameDataManager.Instance.abyss_dungeon_info[i];

                    /*
                    s_abyss_dungeon_work is_work = character.works.abyss_dungeon.Where(x => x.info.name.Contains(work.info.name)).FirstOrDefault();
                    if (is_work != null)
                    {
                        is_work = work;
                    }
                    else
                    {
                        character.works.abyss_dungeon.Add(work);
                    }*/
                    character.works.abyss_dungeon.Add(work);
                }
            }
        }
        public void Add_work_abyss_dungeon(UserCharacterStructor character, s_abyss_dungeon_info info)
        {
            for (int i = 0; i < GameDataManager.Instance.abyss_dungeon_info.Count; i++)
            {
                if ((GameDataManager.Instance.abyss_dungeon_info[i].name != info.name) ||
                    (GameDataManager.Instance.abyss_dungeon_info[i].difficulty != info.difficulty))
                {
                    continue;
                }
                double item_level = character.item_level;
                double offense_power = character.offense_power;

                double item_level_min = GameDataManager.Instance.abyss_dungeon_info[i].level_min;
                double item_level_max = GameDataManager.Instance.abyss_dungeon_info[i].level_max;
                double offense_power_0 = GameDataManager.Instance.abyss_dungeon_info[i].offense_power_0;
                double offense_power_20 = GameDataManager.Instance.abyss_dungeon_info[i].offense_power_20;
                double offense_power_35 = GameDataManager.Instance.abyss_dungeon_info[i].offense_power_35;
                double offense_power_50 = GameDataManager.Instance.abyss_dungeon_info[i].offense_power_50;

                //if (((item_level >= item_level_min) && (item_level < item_level_max)) || ((item_level >= item_level_min) && (item_level_max == 0)))
                {
                    s_abyss_dungeon_work work = new s_abyss_dungeon_work();

                    if ((offense_power >= offense_power_50) && (offense_power_50 != 0)) work.offense_power = "공증 50%";
                    else if ((offense_power >= offense_power_35) && (offense_power_35 != 0)) work.offense_power = "공증 35%";
                    else if ((offense_power >= offense_power_20) && (offense_power_20 != 0)) work.offense_power = "공증 20%";
                    else if ((offense_power >= offense_power_0) && (offense_power_0 != 0)) work.offense_power = "공증 0%";
                    else if ((offense_power_0 == 0) && (offense_power_20 == 0) && (offense_power_35 == 0) && (offense_power_50 == 0)) work.offense_power = "공컷 정보 없음";
                    else work.offense_power = "공컷 미달";

                    work.info = GameDataManager.Instance.abyss_dungeon_info[i];

                    /*
                    s_abyss_dungeon_work is_work = character.works.abyss_dungeon.Where(x => x.info.name.Contains(work.info.name)).FirstOrDefault();
                    if (is_work != null)
                    {
                        is_work = work;
                    }
                    else
                    {
                        character.works.abyss_dungeon.Add(work);
                    }*/
                    character.works.abyss_dungeon.Add(work);
                }
            }
        }
        #endregion
        #region-------------------------------- 아이템 레벨에 따른 가디언 토벌 리스트 업 --------------------------------
        public void Initialize_work_gadian_raid(UserCharacterStructor character)
        {
            character.works.gadian_raid.Clear();
            s_gadian_raid_work work = new s_gadian_raid_work();
 
            for (int i = 0; i < GameDataManager.Instance.gadian_raid_info.Count; i++)
            {
                double item_level = character.item_level;
                double offense_power = character.offense_power;

                double item_level_min = GameDataManager.Instance.gadian_raid_info[i].level_min;
                double item_level_max = GameDataManager.Instance.gadian_raid_info[i].level_max;
                double offense_power_0 = GameDataManager.Instance.gadian_raid_info[i].offense_power_0;
                double offense_power_20 = GameDataManager.Instance.gadian_raid_info[i].offense_power_20;
                double offense_power_35 = GameDataManager.Instance.gadian_raid_info[i].offense_power_35;
                double offense_power_50 = GameDataManager.Instance.gadian_raid_info[i].offense_power_50;

                if (((item_level >= item_level_min) && (item_level < item_level_max)) || ((item_level >= item_level_min) && (item_level_max == 0)))
                {
                    if ((offense_power >= offense_power_50) && (offense_power_50 != 0)) work.offense_power = "공증 50%";
                    else if ((offense_power >= offense_power_35) && (offense_power_35 != 0)) work.offense_power = "공증 35%";
                    else if ((offense_power >= offense_power_20) && (offense_power_20 != 0)) work.offense_power = "공증 20%";
                    else if ((offense_power >= offense_power_0) && (offense_power_0 != 0)) work.offense_power = "공증 0%";
                    else if ((offense_power_0 == 0) && (offense_power_20 == 0) && (offense_power_35 == 0) && (offense_power_50 == 0)) work.offense_power = "공컷 정보 없음";
                    else work.offense_power = "공컷 미달";

                    work.info = GameDataManager.Instance.gadian_raid_info[i];
                }
            }
			character.works.gadian_raid.Add(work);
				
        }
        public void Add_work_gadian_raid(UserCharacterStructor character, s_gadian_raid_info add_info)
        {
            if (add_info == null) return;

            s_gadian_raid_work work = new s_gadian_raid_work();

            for (int i = 0; i < GameDataManager.Instance.gadian_raid_info.Count; i++)
            {
                if (GameDataManager.Instance.gadian_raid_info[i].name != add_info.name) continue;

                double item_level = character.item_level;
                double offense_power = character.offense_power;

                double item_level_min = GameDataManager.Instance.gadian_raid_info[i].level_min;
                double item_level_max = GameDataManager.Instance.gadian_raid_info[i].level_max;
                double offense_power_0 = GameDataManager.Instance.gadian_raid_info[i].offense_power_0;
                double offense_power_20 = GameDataManager.Instance.gadian_raid_info[i].offense_power_20;
                double offense_power_35 = GameDataManager.Instance.gadian_raid_info[i].offense_power_35;
                double offense_power_50 = GameDataManager.Instance.gadian_raid_info[i].offense_power_50;

                //if (((item_level >= item_level_min) && (item_level < item_level_max)) || ((item_level >= item_level_min) && (item_level_max == 0)))
                {
                    if ((offense_power >= offense_power_50) && (offense_power_50 != 0)) work.offense_power = "공증 50%";
                    else if ((offense_power >= offense_power_35) && (offense_power_35 != 0)) work.offense_power = "공증 35%";
                    else if ((offense_power >= offense_power_20) && (offense_power_20 != 0)) work.offense_power = "공증 20%";
                    else if ((offense_power >= offense_power_0) && (offense_power_0 != 0)) work.offense_power = "공증 0%";
                    else if ((offense_power_0 == 0) && (offense_power_20 == 0) && (offense_power_35 == 0) && (offense_power_50 == 0)) work.offense_power = "공컷 정보 없음";
                    else work.offense_power = "공컷 미달";

                    work.info = GameDataManager.Instance.gadian_raid_info[i];
                }
            }
            character.works.gadian_raid.Add(work);

        }
        #endregion
        #region-------------------------------- 아이템 레벨에 따른 카오스 던전 리스트 업 --------------------------------
        public void Initialize_work_chaos_dungeon(UserCharacterStructor character)
        {
            character.works.chaos_dungeon.Clear();
            s_chaos_dungeon_work work = new s_chaos_dungeon_work();

            for (int i = 0; i < GameDataManager.Instance.chaos_dungeon_info.Count; i++)
            {
                double item_level = character.item_level;
                double offense_power = character.offense_power;
                
                double item_level_min = GameDataManager.Instance.chaos_dungeon_info[i].level_min;
                double item_level_max = GameDataManager.Instance.chaos_dungeon_info[i].level_max;
                double offense_power_0 = GameDataManager.Instance.chaos_dungeon_info[i].offense_power_0;
                double offense_power_20 = GameDataManager.Instance.chaos_dungeon_info[i].offense_power_20;
                double offense_power_35 = GameDataManager.Instance.chaos_dungeon_info[i].offense_power_35;
                double offense_power_50 = GameDataManager.Instance.chaos_dungeon_info[i].offense_power_50;

                if (((item_level >= item_level_min) && (item_level < item_level_max)) || ((item_level >= item_level_min) && (item_level_max == 0)))
                {
                    
                    if ((offense_power >= offense_power_50) && (offense_power_50 != 0)) work.offense_power = "공증 50%";
                    else if ((offense_power >= offense_power_35) && (offense_power_35 != 0)) work.offense_power = "공증 35%";
                    else if ((offense_power >= offense_power_20) && (offense_power_20 != 0)) work.offense_power = "공증 20%";
                    else if ((offense_power >= offense_power_0) && (offense_power_0 != 0)) work.offense_power = "공증 0%";
                    else if ((offense_power_0 == 0) && (offense_power_20 == 0) && (offense_power_35 == 0) && (offense_power_50 == 0)) work.offense_power = "공컷 정보 없음";
                    else work.offense_power = "공컷 미달";

                    work.info = GameDataManager.Instance.chaos_dungeon_info[i];
                }
            }
            character.works.chaos_dungeon.Add(work);
        }
        public void Add_work_chaos_dungeon(UserCharacterStructor character, s_chaos_dungeon_info add_info)
        {
            if (add_info == null) return;

            s_chaos_dungeon_work work = new s_chaos_dungeon_work();

            for (int i = 0; i < GameDataManager.Instance.chaos_dungeon_info.Count; i++)
            {
                if ((GameDataManager.Instance.chaos_dungeon_info[i].name != add_info.name) ||
                    (GameDataManager.Instance.chaos_dungeon_info[i].difficulty != add_info.difficulty))
                {
                    continue;
                }
                double item_level = character.item_level;
                double offense_power = character.offense_power;

                double item_level_min = GameDataManager.Instance.chaos_dungeon_info[i].level_min;
                double item_level_max = GameDataManager.Instance.chaos_dungeon_info[i].level_max;
                double offense_power_0 = GameDataManager.Instance.chaos_dungeon_info[i].offense_power_0;
                double offense_power_20 = GameDataManager.Instance.chaos_dungeon_info[i].offense_power_20;
                double offense_power_35 = GameDataManager.Instance.chaos_dungeon_info[i].offense_power_35;
                double offense_power_50 = GameDataManager.Instance.chaos_dungeon_info[i].offense_power_50;

                {
                    if ((offense_power >= offense_power_50) && (offense_power_50 != 0)) work.offense_power = "공증 50%";
                    else if ((offense_power >= offense_power_35) && (offense_power_35 != 0)) work.offense_power = "공증 35%";
                    else if ((offense_power >= offense_power_20) && (offense_power_20 != 0)) work.offense_power = "공증 20%";
                    else if ((offense_power >= offense_power_0) && (offense_power_0 != 0)) work.offense_power = "공증 0%";
                    else if ((offense_power_0 == 0) && (offense_power_20 == 0) && (offense_power_35 == 0) && (offense_power_50 == 0)) work.offense_power = "공컷 정보 없음";
                    else work.offense_power = "공컷 미달";

                    work.info = GameDataManager.Instance.chaos_dungeon_info[i];
                }
            }
            character.works.chaos_dungeon.Add(work);
        }
        #endregion
        #region-------------------------------- 아이템 레벨에 따른 에포나 리스트 업 --------------------------------
        public void Initialize_work_epona_quest(UserCharacterStructor character)
        {
            character.works.epona_quest.Clear();
            for (int i = 0; i < 3; i++)
            {
                s_epona_quest_work work = new s_epona_quest_work();
                work.info = new s_epona_quest_info();
                work.info.quest_name = "";
                work.is_done = false;
                work.is_ignore = false;
                character.works.epona_quest.Add(work);
            }
        }
        public void Add_work_epona_quest(UserCharacterStructor character)
        {
            s_epona_quest_work work = new s_epona_quest_work();
            work.info = new s_epona_quest_info();
            work.info.quest_name = "";
            work.is_done = false;
            work.is_ignore = false;
            character.works.epona_quest.Add(work);
        }
        #endregion

        public void Initialize_character_works(UserCharacterStructor character)
        {
            if (character == null) return;
            Initialize_work_abyss_raid(character);
            Initialize_work_gadian_raid(character);
            Initialize_work_abyss_dungeon(character);
            Initialize_work_chaos_dungeon(character);
            Initialize_work_epona_quest(character);
            Initialize_work_challenge_gadian_raid(character);
            Initialize_work_challenge_abyss_dungeon(character);
            Initialize_work_guild_raid(character);

        }

    }
}
