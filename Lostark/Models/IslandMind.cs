﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lostark.Models
{
    public class s_IslandMind
    {
        public int index { get; set; }
        public string name { get; set; }
        public string note { get; set; }
        public bool is_done { get; set; }

    }
}
