﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lostark.Models
{
    public class GameDataManager
    {
        //private static 인스턴스 객체
        private static readonly Lazy<GameDataManager> instance = new Lazy<GameDataManager>(() => new GameDataManager());
        //public static 의 객체반환 함수
        public static GameDataManager Instance { get { return instance.Value; } }

        const string game_config_path = "game_info/";

        const string job_list_path = game_config_path + "job_list.csv";
        const string abyss_dungeon_info_path = game_config_path + "abyss_dungeon_info.csv";
        const string abyss_raid_info_path = game_config_path + "abyss_raid_info.csv";
        const string gadian_raid_info_path = game_config_path + "gadian_raid_info.csv";
        const string chaos_dungeon_info_path = game_config_path + "chaos_dungeon.csv";
        const string Island_info_path = game_config_path + "island.csv";
        const string ch_abyss_dungeon_info_path = game_config_path + "challenge_abyss_dungeon_info.csv";
        const string ch_gadian_raid_info_path = game_config_path + "challenge_gadian_raid_info.csv";
        const string guild_raid_info_path = game_config_path + "guild_raid_info.csv";

        public List<s_job_info> job_info;
        public List<s_abyss_dungeon_info> abyss_dungeon_info;
        public List<s_abyss_raid_info> abyss_raid_info;
        public List<s_gadian_raid_info> gadian_raid_info;
        public List<s_chaos_dungeon_info> chaos_dungeon_info;
        public List<s_IslandMind> island_mind_info;
        public List<s_challenge_gadian_raid_info> challenge_gadian_raid_info;
        public List<s_challenge_abyss_dungeon_info> challenge_abyss_dungeon_info;
        public List<s_guild_raid_info> guild_raid_info;

        

        private GameDataManager()
        {
        }

        private void initialize_job_info()
        {
            using (var reader = new StreamReader(job_list_path))
            using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
            {
                var records = csv.GetRecords<s_job_info>();
                job_info = records.ToList();
            }
        }
        private void initialize_abyss_dungeon_info()
        {
            using (var reader = new StreamReader(abyss_dungeon_info_path))
            using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
            {
                var records = csv.GetRecords<s_abyss_dungeon_info>();
                abyss_dungeon_info = records.ToList();
            }
        }
        private void initialize_abyss_raid_info()
        {
            using (var reader = new StreamReader(abyss_raid_info_path))
            using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
            {
                var records = csv.GetRecords<s_abyss_raid_info>();
                abyss_raid_info = records.ToList();
            }
        }
        private void initialize_gadian_raid_info()
        {
            using (var reader = new StreamReader(gadian_raid_info_path))
            using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
            {
                var records = csv.GetRecords<s_gadian_raid_info>();
                gadian_raid_info = records.ToList();
            }
        }
        private void initialize_chaos_dungeon_info()
        {
            using (var reader = new StreamReader(chaos_dungeon_info_path))
            using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
            {
                var records = csv.GetRecords<s_chaos_dungeon_info>();
                chaos_dungeon_info = records.ToList();
            }
        }

        private void initialize_Island_Info()
        {
            try
            {
                using (var reader = new StreamReader(Island_info_path))
                using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                {
                    var records = csv.GetRecords<s_IslandMind>();
                    island_mind_info = records.ToList();
                }
            }
            catch
            {
                
            }
        }
        private void initialize_challenge_abyss_dungeon_Info()
        {
            try
            {
                using (var reader = new StreamReader(ch_abyss_dungeon_info_path))
                using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                {
                    var records = csv.GetRecords<s_challenge_abyss_dungeon_info>();
                    challenge_abyss_dungeon_info = records.ToList();
                }
            }
            catch
            {

            }
        }
        private void initialize_challenge_gadian_raid_Info()
        {
            try
            {
                using (var reader = new StreamReader(ch_gadian_raid_info_path))
                using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                {
                    var records = csv.GetRecords<s_challenge_gadian_raid_info>();
                    challenge_gadian_raid_info = records.ToList();
                }
            }
            catch
            {

            }
        }

        private void initialize_guild_raid_Info()
        {
            try
            {
                using (var reader = new StreamReader(guild_raid_info_path))
                using (var csv = new CsvReader(reader, CultureInfo.InvariantCulture))
                {
                    var records = csv.GetRecords<s_guild_raid_info>();
                    guild_raid_info = records.ToList();
                }
            }
            catch
            {

            }
        }

        public void Initialize()
        {
            initialize_job_info();
            initialize_abyss_dungeon_info();
            initialize_abyss_raid_info();
            initialize_gadian_raid_info();
            initialize_chaos_dungeon_info();
            initialize_Island_Info();
            initialize_challenge_abyss_dungeon_Info();
            initialize_challenge_gadian_raid_Info();
            initialize_guild_raid_Info();
        }

                
    }
}
