﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Configuration;
using System.Data;
using System.Collections.ObjectModel;

namespace Lostark.Models
{
    public class CharacterInfo
    {
        public string name;
        public double item_level;

        public CharacterInfo(string _name, double _item_level)
        {
            name = _name;
            item_level = _item_level;
        }
    }

    public class DatabaseManager
    {

        //private static 인스턴스 객체
        private static readonly Lazy<DatabaseManager> instance = new Lazy<DatabaseManager>(() => new DatabaseManager());
        //public static 의 객체반환 함수
        public static DatabaseManager Instance { get { return instance.Value; } }
        
        private DatabaseManager()
        {
        }

        public void Initialize()
        {
        }
        /*
        public void UpdateCharacterInfo()
        {

        }
        public void RemoveCharacter(string name, string job, double item_level)
        {
            string con_string = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;
            int guid = Guid.NewGuid().GetHashCode();
            try
            {
                using (SqlConnection con = new SqlConnection(con_string))
                {
                    con.Open();
                    string qry = $"insert into tb_char_info(id, name,  job, item_level) values({guid}, N'{name}', N'{job}',{item_level})";
                    using (SqlCommand cmd = new SqlCommand(qry, con))
                    {
                        int result = cmd.ExecuteNonQuery();
                        if (result == 1)
                        {
                            //MessageBox.Show("excute query");
                        }
                    }
                }
            }
            catch (System.Data.SqlClient.SqlException e)
            {
                MessageBox.Show($"error:  {e}");
            }
        }

        private void AddCharacter(string name, string job, double item_level)
        {
            string con_string = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;
            int guid = Guid.NewGuid().GetHashCode();
            try
            {
                using (SqlConnection con = new SqlConnection(con_string))
                {
                    con.Open();
                    string qry = $"insert into tb_char_info(name,  job, item_level) values(N'{name}', N'{job}',{item_level})";
                    using (SqlCommand cmd = new SqlCommand(qry, con))
                    {
                        int result = cmd.ExecuteNonQuery();
                        if (result == 1)
                        {
                            //MessageBox.Show("excute query");
                        }
                    }
                }
            }
            catch (System.Data.SqlClient.SqlException e)
            {
                MessageBox.Show($"error:  {e}");
            }
        }
        private void DeleteCharacterInfo()
        {
            string con_string = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;
            try
            {
                using (SqlConnection con = new SqlConnection(con_string))
                {
                    con.Open();
                    string qry = $"delete from tb_char_info";

                    using (SqlCommand cmd = new SqlCommand(qry, con))
                    {
                        cmd.ExecuteNonQuery();
                    }
                }
            }
            catch (System.Data.SqlClient.SqlException e)
            {
                MessageBox.Show($"error:  {e}");
            }
        }


        public DataTable GetCharacterInfo()
        {
            string con_string = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;
            DataTable dt = new DataTable();
            try
            {
                using (SqlConnection con = new SqlConnection(con_string))
                {
                    con.Open();
                    string qry = $"select name, job, item_level from tb_char_info";
                    
                    using (SqlCommand cmd = new SqlCommand(qry, con))
                    {
                        SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                        adapter.Fill(dt);
                    }
                }
            }
            catch (System.Data.SqlClient.SqlException e)
            {
                MessageBox.Show($"error:  {e}");
            }

            return dt;
        }

        public void UpdateCharacterInfo(DataTable dt)
        {
            string con_string = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;
            try
            {
                DeleteCharacterInfo();

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    AddCharacter(dt.Rows[i]["name"].ToString(), dt.Rows[i]["job"].ToString(), Convert.ToDouble(dt.Rows[i]["item_level"].ToString()));
                }
            }
            catch (System.Data.SqlClient.SqlException e)
            {
                MessageBox.Show($"error:  {e}");
            }
        }

        public ObservableCollection<CharacterStructor> GetCharacterInfo2()
        {
            string con_string = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;
            ObservableCollection<CharacterStructor> dt = new ObservableCollection<CharacterStructor>();
            try
            {
                using (SqlConnection con = new SqlConnection(con_string))
                {
                    con.Open();
                    string qry = $"select name, job, item_level from tb_char_info";

                    using (SqlCommand cmd = new SqlCommand(qry, con))
                    {
                        SqlDataReader reader = cmd.ExecuteReader();
                        while (reader.Read())
                        {
                            string name = reader.GetString(0);
                            string job = reader.GetString(1);
                            double item_level = reader.GetDouble(2);
                            //adding row data to observable
                            dt.Add(new CharacterStructor(name, job, item_level));
                        }
                    }
                }
            }
            catch (System.Data.SqlClient.SqlException e)
            {
                MessageBox.Show($"error:  {e}");
            }

            return dt;
        }

        public void UpdateCharacterInfo2(ObservableCollection<CharacterStructor> dt)
        {
            string con_string = ConfigurationManager.ConnectionStrings["ConString"].ConnectionString;
            try
            {
                DeleteCharacterInfo();

                for (int i = 0; i < dt.Count; i++)
                {
                    AddCharacter(dt[i].name, dt[i].job, dt[i].item_level);
                }
            }
            catch (System.Data.SqlClient.SqlException e)
            {
                MessageBox.Show($"error:  {e}");
            }
        }*/

    }
}
