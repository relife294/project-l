﻿using GalaSoft.MvvmLight.Messaging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Lostark.Models
{
    public class Weekly_Work
    {
        public int index { get; set; }
        public string item { get; set; }
        public string name { get; set; }
        public string name2 { get; set; }
        public bool is_done { get; set; }
        public bool is_ignored { get; set; }
        public string effect { get; set; }
        public Weekly_Work()
        {

        }
    }
    public class Daily_Work
    {
        public int index { get; set; }
        public string item { get; set; }

        public string name { get; set; }
        public string name2 { get; set; }
        public bool is_done { get; set; }
        public bool is_ignored { get; set; }
        public string effect { get; set; }
        public Daily_Work()
        {

        }
    }

    public class UserCharacterStructor
    {
        public string name { get; set; }
        public double item_level { get; set; }
        public s_job_info job { get; set; }
        public double offense_power { get; set; }

        public int tier { get; set; }
        public CharacterWorks works { get; set; }

        List<Daily_Work> daily_Work;
        List<Weekly_Work> weekly_Works;

        public UserCharacterStructor(string name, s_job_info job, double item_level, double offense_power, int tier)
        {
            works = new CharacterWorks();
            daily_Work = new List<Daily_Work>();
            weekly_Works = new List<Weekly_Work>();
            this.name = name;
            this.item_level = item_level;
            this.job = job;
            this.offense_power = offense_power;
            this.tier = tier;
        }

        public UserCharacterStructor()
        {
            works = new CharacterWorks();
            daily_Work = new List<Daily_Work>();
            weekly_Works = new List<Weekly_Work>();
        }

        public void ClearDailyWorkDone()
        {
            foreach(var value in daily_Work)
            {
                value.is_done = false;
                SetDailyWorks(value);
            }
        }
        public void ClearWeeklyWorkDone()
        {
            foreach (var value in weekly_Works)
            {
                value.is_done = false;
                SetWeeklyWorks(value);
            }
        }

        public bool GetDailyDone()
        {
            for (int i = 0; i < daily_Work.Count; i++)
            {
                if (daily_Work[i].is_done == false) return false;
            }
            return true;
        }
        public List<Daily_Work> GetDailyWorks()
        {
            daily_Work.Clear();

            try
            {
                for (int i = 0; i < works.epona_quest.Count; i++)
                {
                    Daily_Work w = new Daily_Work();
                    w.index = i;
                    w.item = "에포나";
                    w.name = works.epona_quest[i].info.quest_name;
                    w.name2 = "";
                    w.effect = "";
                    w.is_done = works.epona_quest[i].is_done;
                    w.is_ignored = works.epona_quest[i].is_ignore;
                    daily_Work.Add(w);
                }
                for (int i = 0; i < works.gadian_raid.Count; i++)
                {
                    Daily_Work w = new Daily_Work();
                    w.item = "가디언토벌";
                    w.name = works.gadian_raid[i].info.name;
                    w.name2 = "";
                    w.effect = works.gadian_raid[i].offense_power;
                    w.is_done = works.gadian_raid[i].is_done;
                    w.is_ignored = works.gadian_raid[i].is_ignore;
                    daily_Work.Add(w);
                }
                for (int i = 0; i < works.chaos_dungeon.Count; i++)
                {
                    Daily_Work w = new Daily_Work();
                    w.item = "카오스던전";
                    w.name = works.chaos_dungeon[i].info.name;
                    w.name2 = works.chaos_dungeon[i].info.difficulty;
                    w.effect = works.chaos_dungeon[i].offense_power;
                    w.is_done = works.chaos_dungeon[i].is_done;
                    w.is_ignored = works.chaos_dungeon[i].is_ignore;
                    daily_Work.Add(w);
                }

            }
            catch
            {

            }
            works.UpdateWorkDone();
            return daily_Work;
        }
        public void DeleteDailyWorks(Daily_Work work)
        {
            daily_Work.Remove(work);
            if (work.item == "에포나")
            {
                works.epona_quest.RemoveAt(work.index);
            }
            else if (work.item == "가디언토벌")
            {
                works.gadian_raid.RemoveAt(work.index);
            }
            else if (work.item == "카오스던전")
            {
                works.chaos_dungeon.RemoveAt(work.index);
            }
            works.UpdateWorkDone();
        }
        public void SetDailyWorks(Daily_Work work)
        {
            if (work == null) return;

            if (work.item == "에포나")
            {
                works.epona_quest[work.index].is_done = work.is_done;
                works.epona_quest[work.index].is_ignore = work.is_ignored;
            }
            else if (work.item == "가디언토벌")
            {
                works.gadian_raid[work.index].is_done = work.is_done;
                works.gadian_raid[work.index].is_ignore = work.is_ignored;
            }
            else if (work.item == "카오스던전")
            {
                works.chaos_dungeon[work.index].is_done = work.is_done;
                works.chaos_dungeon[work.index].is_ignore = work.is_ignored;
            }
            works.UpdateWorkDone();
        }

        public void AddDailyWorks(Daily_Work work)
        {
            if (work == null) return;

            if (work.item == "에포나")
            {
                UserDataManager.Instance.Add_work_epona_quest(this);
            }
            else if (work.item == "가디언토벌")
            {
                s_gadian_raid_info info = new s_gadian_raid_info()
                {
                    name = work.name,
                };
                UserDataManager.Instance.Add_work_gadian_raid(this, info);
            }
            else if (work.item == "카오스던전")
            {
                s_chaos_dungeon_info info = new s_chaos_dungeon_info()
                {
                    name = work.name,
                    difficulty = work.name2,
                };
                UserDataManager.Instance.Add_work_chaos_dungeon(this, info);
            }
            works.UpdateWorkDone();
        }

        public List<Weekly_Work> GetWeeklyWorks()
        {
            weekly_Works.Clear();

            for (int i = 0; i < works.abyss_dungeon.Count; i++)
            {
                Weekly_Work w = new Weekly_Work();
                w.index = i;
                w.item = "어비스던전";
                w.name = works.abyss_dungeon[i].info.name;
                w.name2 = works.abyss_dungeon[i].info.difficulty;
                w.effect = works.abyss_dungeon[i].offense_power;
                w.is_done = works.abyss_dungeon[i].is_done;
                w.is_ignored = works.abyss_dungeon[i].is_ignore;
                weekly_Works.Add(w);
            }
            for (int i = 0; i < works.abyss_raid.Count; i++)
            {
                Weekly_Work w = new Weekly_Work();
                w.index = i;
                w.item = "어비스레이드";
                w.name = works.abyss_raid[i].info.name;
                w.name2 = works.abyss_raid[i].info.phase;
                w.effect = works.abyss_raid[i].offense_power;
                w.is_done = works.abyss_raid[i].is_done;
                w.is_ignored = works.abyss_raid[i].is_ignore;
                weekly_Works.Add(w);
            }
            for (int i = 0; i < works.challenge_adyss_dungeon.Count; i++)
            {
                Weekly_Work w = new Weekly_Work();
                w.index = i;
                w.item = "도전 어비스던전";
                w.name = works.challenge_adyss_dungeon[i].info.name;
                w.name2 = "";
                w.effect = "";
                w.is_done = works.challenge_adyss_dungeon[i].is_done;
                w.is_ignored = works.challenge_adyss_dungeon[i].is_ignore;
                weekly_Works.Add(w);
            }
            for (int i = 0; i < works.challenge_gadian_raid.Count; i++)
            {
                Weekly_Work w = new Weekly_Work();
                w.index = i;
                w.item = "도전 가디언토벌";
                w.name = works.challenge_gadian_raid[i].info.name;
                w.name2 = "";
                w.effect = "";
                w.is_done = works.challenge_gadian_raid[i].is_done;
                w.is_ignored = works.challenge_gadian_raid[i].is_ignore;
                weekly_Works.Add(w);
            }
            for (int i = 0; i < works.guild_raid.Count; i++)
            {
                Weekly_Work w = new Weekly_Work();
                w.index = i;
                w.item = "길드토벌";
                w.name = works.guild_raid[i].info.name;
                w.name2 = "";
                w.effect = "";
                w.is_done = works.guild_raid[i].is_done;
                w.is_ignored = works.guild_raid[i].is_ignore;
                weekly_Works.Add(w);
            }
            works.UpdateWorkDone();
            return weekly_Works;
        }
        public void SetWeeklyWorks(Weekly_Work work)
        {
            if (work.item == "어비스던전")
            {
                works.abyss_dungeon[work.index].is_done = work.is_done;
            }
            else if (work.item == "어비스레이드")
            {
                works.abyss_raid[work.index].is_done = work.is_done;
            }
            else if (work.item == "도전 어비스던전")
            {
                works.challenge_adyss_dungeon[work.index].is_done = work.is_done;
            }
            else if (work.item == "도전 가디언토벌")
            {
                works.challenge_gadian_raid[work.index].is_done = work.is_done;
            }
            else if (work.item == "길드토벌")
            {
                works.guild_raid[work.index].is_done = work.is_done;
            }
            works.UpdateWorkDone();
        }
        public void AddWeeklyWorks(Weekly_Work work)
        {
            if (work.item == "어비스던전")
            {
                s_abyss_dungeon_info info = new s_abyss_dungeon_info()
                {
                    name = work.name,
                    difficulty = work.name2,
                };
                UserDataManager.Instance.Add_work_abyss_dungeon(this, info);
            }
            else if (work.item == "어비스레이드")
            {
                s_abyss_raid_info info = new s_abyss_raid_info()
                {
                    name = work.name,
                    phase = work.name2,
                };
                UserDataManager.Instance.Add_work_abyss_raid(this, info);
            }
            else if (work.item == "도전 어비스던전")
            {
            }
            else if (work.item == "도전 가디언토벌")
            {

            }
            else if (work.item == "길드토벌")
            {
            }

            works.UpdateWorkDone();
        }
        public void DeleteWeeklyWorks(Weekly_Work work)
        {
            weekly_Works.Remove(work);
            if (work.item == "어비스던전")
            {
                works.abyss_dungeon.RemoveAt(work.index);
            }
            else if (work.item == "어비스레이드")
            {
                works.abyss_raid.RemoveAt(work.index);
            }
            else if (work.item == "도전 어비스던전")
            {
                works.challenge_adyss_dungeon.RemoveAt(work.index);
            }
            else if (work.item == "도전 가디언토벌")
            {
                works.challenge_gadian_raid.RemoveAt(work.index);
            }
            else if (work.item == "길드토벌")
            {
                works.guild_raid.RemoveAt(work.index);
            }
            works.UpdateWorkDone();
        }
    }
}
