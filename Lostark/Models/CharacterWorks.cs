﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Policy;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Lostark.Models
{
    public class s_epona_quest_work
    {
        public s_epona_quest_info info { get; set; }

        public bool is_done { get; set; }
        public bool is_ignore { get; set; }

        public s_epona_quest_work()
        {

        }
    }

    public class s_gadian_raid_work
    {
        public s_gadian_raid_info info { get; set; }
        public bool is_done { get; set; }
        public bool is_ignore { get; set; }

        public string offense_power { get; set; }

        public s_gadian_raid_work(s_gadian_raid_info info, bool is_done, bool is_ignore, string offense_power)
        {
            this.info = info;
            this.is_done = is_done;
            this.is_ignore = is_ignore;
            this.offense_power = offense_power;
        }

        public s_gadian_raid_work()
        {

        }
    }

    public class s_abyss_raid_work
    {
        public s_abyss_raid_info info { get; set; }
        public bool is_done { get; set; }
        public bool is_ignore { get; set; }

        public string offense_power { get; set; }

        public s_abyss_raid_work(s_abyss_raid_info info, bool is_done, bool is_ignore, string offense_power)
        {
            this.info = info;
            this.is_done = is_done;
            this.is_ignore = is_ignore;
            this.offense_power = offense_power;
        }

        public s_abyss_raid_work()
        {

        }
    }

    public class s_abyss_dungeon_work
    {

        public s_abyss_dungeon_info info { get; set; }
        public bool is_done { get; set; }
        public bool is_ignore { get; set; }

        public string offense_power { get; set; }


        public s_abyss_dungeon_work(s_abyss_dungeon_info info, bool is_done, bool is_ignore, string offense_power)
        {
            this.info = info;
            this.is_done = is_done;
            this.is_ignore = is_ignore;
            this.offense_power = offense_power;
        }
        public s_abyss_dungeon_work()
        {

        }
    }

    public class s_chaos_dungeon_work
    {
        public s_chaos_dungeon_info info { get; set; }
        public bool is_done { get; set; }
        public bool is_ignore { get; set; }

        public string offense_power { get; set; }


        public s_chaos_dungeon_work(s_chaos_dungeon_info chaos_dungeon, bool is_done, bool is_ignore, string offense_power)
        {
            this.info = info;
            this.is_done = is_done;
            this.is_ignore = is_ignore;
            this.offense_power = offense_power;
        }
        public s_chaos_dungeon_work()
        {
        }
    }


    public class s_guild_raid_work
    {
        public s_guild_raid_info info { get; set; }
        public bool is_done { get; set; }
        public bool is_ignore { get; set; }

        public string offense_power { get; set; }

        public s_guild_raid_work()
        {
        }
    }
    public class s_challenge_gadian_raid_work
    {
        public s_challenge_gadian_raid_info info { get; set; }
        public bool is_done { get; set; }
        public bool is_ignore { get; set; }

        public string offense_power { get; set; }

        public s_challenge_gadian_raid_work()
        {
        }
    }
    public class s_challenge_abyss_dungeon_work
    {
        public s_challenge_abyss_dungeon_info info { get; set; }
        public bool is_done { get; set; }
        public bool is_ignore { get; set; }

        public string offense_power { get; set; }

        public s_challenge_abyss_dungeon_work()
        {
        }
    }

    public class CharacterWorks
    {
        public bool is_done { get; set; }
        public bool is_daily_done { get; set; }
        public bool is_weekly_done { get; set; }
        public List<s_gadian_raid_work> gadian_raid { get; set; }
        public List<s_chaos_dungeon_work>  chaos_dungeon { get; set; }
        public List<s_abyss_raid_work> abyss_raid { get; set; }
        public List<s_abyss_dungeon_work> abyss_dungeon { get; set; }

        //------------------------------------------------------------------------------------
        public List<s_epona_quest_work> epona_quest { get; set; }
        public List<s_challenge_gadian_raid_work> challenge_gadian_raid { get; set; }
        public List<s_challenge_abyss_dungeon_work> challenge_adyss_dungeon { get; set; }
        public List<s_guild_raid_work> guild_raid { get; set; }
        //------------------------------------------------------------------------------------
        public CharacterWorks()
        {
            abyss_raid = new List<s_abyss_raid_work>();
            abyss_dungeon = new List<s_abyss_dungeon_work>();
            gadian_raid = new List<s_gadian_raid_work>();
            epona_quest = new List<s_epona_quest_work>();
            chaos_dungeon = new List<s_chaos_dungeon_work>();
            challenge_gadian_raid = new List<s_challenge_gadian_raid_work>();
            challenge_adyss_dungeon = new List<s_challenge_abyss_dungeon_work>();
            guild_raid = new List<s_guild_raid_work>();
            is_done = false;

        }

        public void UpdateWorkDone()
        {
            is_daily_done = true;
            foreach(var val in gadian_raid)
            {
                if (val.is_done == false) is_daily_done = false;
            }
            foreach (var val in epona_quest)
            {
                if (val.is_done == false) is_daily_done = false;
            }
            foreach (var val in chaos_dungeon)
            {
                if (val.is_done == false) is_daily_done = false;
            }


            is_weekly_done = true;
            foreach (var val in abyss_dungeon)
            {
                if (val.is_done == false) is_weekly_done = false;
            }
            foreach (var val in abyss_raid)
            {
                if (val.is_done == false) is_weekly_done = false;
            }
            foreach (var val in guild_raid)
            {
                if (val.is_done == false) is_weekly_done = false;
            }
            foreach (var val in challenge_gadian_raid)
            {
                if (val.is_done == false) is_weekly_done = false;
            }
            foreach (var val in challenge_adyss_dungeon)
            {
                if (val.is_done == false) is_weekly_done = false;
            }
            if ((is_daily_done == true) && (is_weekly_done == true)) is_done = true;
            else is_done = false;

        }

    }
}
