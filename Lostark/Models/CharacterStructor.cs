﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lostark.Models
{
    public class CharacterStructor
    {
        public string name { get; set; }
        public string job { get; set; }
        public double item_level { get; set; }

        public CharacterStructor(string _name, string _job, double _item_level)
        {
            name = _name;
            job = _job;
            item_level = _item_level;
        }

    }
}
