﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using AutoUpdaterDotNET;
using System.Reflection;
using System.Threading;
using System.Globalization;
using System.Windows.Threading;
using System.Net;

namespace Lostark
{
    /// <summary>
    /// MainWindow.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public MainWindow()
        {
            InitializeComponent();
            
            Assembly assembly = Assembly.GetEntryAssembly();
            LabelVersion.Content = $"Ver {assembly.GetName().Version}";
            Thread.CurrentThread.CurrentCulture =
                Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture("en");
            AutoUpdater.LetUserSelectRemindLater = false;
            AutoUpdater.ShowRemindLaterButton = false;
            AutoUpdater.ReportErrors = false;
            AutoUpdater.ShowSkipButton = false;

        
            //AutoUpdater.Start("http://rbsoft.org/updates/AutoUpdaterTestWPF.xml");
            AutoUpdater.Start("http://34.64.237.104:8080/project-l/project-l/AutoUpdater.xml");
            //AutoUpdater.Start("ftp://34.64.237.104/home/project-l-master/project-l/AutoUpdaterTest.xml", new NetworkCredential("FtpUserName", "FtpPassword"))
        }
    }
}
