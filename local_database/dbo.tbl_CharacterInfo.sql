﻿CREATE TABLE [dbo].[tbl_CharacterInfo] (
    [Id]         INT        NOT NULL,
    [name]       NTEXT      NOT NULL,
    [item_level] FLOAT (53) NULL,
    PRIMARY KEY CLUSTERED ([name])
);

